__author__ = 'shubham'

file_name = "2001"
input_name = 'data_file/National/'+file_name+'.csv'
output_name = 'data_file/National/'+file_name+'_c.csv'

# Open a file
fo = open(output_name, "wb")
with open(input_name) as f:
    for line in f:
        content = line.replace("'", "")
        content = content.replace('"', '')
        fo.write(content)
# Close opened file
fo.close()
