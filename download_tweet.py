# Written while listening to The Proclaimers
# by Shubham Goel

__author__ = 'shubham@ischool.berkeley.edu'

from ConfigParser import SafeConfigParser
import twitter
import csv
import time
import requests
import sys
from pymongo import MongoClient

""" Global Variables """
config_file = 'config/config.ini'

""" data files """
input_file = None
source_name = None
credentials_file = None
error_log = None

""" database connections """
db_location = None
db_port = None
db_name = None
client = None
db = None
tweets = None
users = None
hashtags = None
sources = None

""" twitter api """
credentials = []  # stores all the authenticated credentials
expiration_time = []  # time at which api expired


""" api switch management """


def set_expired(api):
    print "def set_expired:"
    cred_index = credentials.index(api)
    print "expired api index: "+str(cred_index)
    expiration_time[cred_index] = int(time.time())


def get_active_api():
    print "def get_active_api"
    min_expired_time = min(expiration_time)
    # 15 mins since expiration
    if (int(time.time())-min_expired_time) < 900:
        print "sleeping for: "+str(960-(int(time.time())-min_expired_time))+" sec"
        time.sleep(960-(int(time.time())-min_expired_time) + 60)
    cred_index = expiration_time.index(min_expired_time)
    return credentials[cred_index]


""" Save objects """


def save_tweet(tweet):
    """
    save the downloaded tweet from the api to mongo db
    """
    tweet_dict = dict()
    tweet_id = tweet.GetId()
    tweet_dict["_id"] = tweet.GetId()
    tweet_user_obj = tweet.GetUser()

    tweet_dict["user_id"] = tweet_user_obj.GetId()

    tweet_text = tweet.GetText()
    tweet_dict["text"] = tweet_text.encode('utf8')
    tweet_dict["truncated"] = tweet.truncated
    tweet_dict["created_at"] = tweet.GetCreatedAtInSeconds()

    tweet_dict["favorited"] = tweet.GetFavorited()
    tweet_dict["favorite_count"] = tweet.GetFavoriteCount()
    tweet_dict["retweet_count"] = tweet.GetRetweetCount()

    tweet_dict["geo"] = tweet.GetGeo()
    tweet_dict["place"] = tweet.GetPlace()
    tweet_dict["location"] = tweet.GetLocation()
    tweet_dict["coordinates"] = tweet.GetCoordinates()

    tweet_dict["lang"] = tweet.lang
    tweet_dict["source"] = tweet.GetSource()
    tweet_dict["in_reply_to_screen_name"] = tweet.GetInReplyToScreenName()
    tweet_dict["in_reply_to_user_id"] = tweet.GetInReplyToUserId()
    tweet_dict["in_reply_to_status_id"] = tweet.GetInReplyToStatusId()

    tweet_urls = [u.url for u in tweet.urls]
    tweet_dict["urls"] = ', '.join(tweet_urls)

    tweet_user_mentions = [str(m.id) for m in tweet.user_mentions]
    tweet_dict["user_mentions"] = ', '.join(tweet_user_mentions)

    tweets.insert(tweet_dict)

    for h in tweet.hashtags:
        tag = dict()
        tag["hashtag"] = h.text.encode('utf8')
        tag["tweet_id"] = tweet_id
        hashtags.insert(tag)


def save_user(user):
    """
    save downloaded user object from the api to mongo db
    """
    # strings
    user_dict = dict()
    user_dict["_id"] = user.GetId()
    user_dict["name"] = user.GetName()
    user_dict["screen_name"] = user.GetScreenName()
    user_dict["description"] = user.GetDescription().encode('utf8')
    user_dict["profile_image_url"] = user.GetProfileImageUrl()
    user_dict["protected"] = user.GetProtected()
    user_dict["utc_offset"] = user.GetUtcOffset()
    user_dict["time_zone"] = user.GetTimeZone()
    user_dict["location"] = user.GetLocation()

    user_dict["url"] = user.GetUrl()
    user_dict["friends_count"] = user.GetFriendsCount()
    user_dict["listed_count"] = user.GetListedCount()
    user_dict["followers_count"] = user.GetFollowersCount()
    user_dict["statuses_count"] = user.GetStatusesCount()
    user_dict["favourites_count"] = user.GetFavouritesCount()
    user_dict["geo_enabled"] = user.GetGeoEnabled()
    user_dict["verified"] = user.GetVerified()
    user_dict["lang"] = user.GetLang()
    user_dict["created_at"] = user.GetCreatedAt()
    users.insert(user_dict)


def read_config(config_file):
    """
    reading configurations and updating the global variables
    """
    settings_parser = SafeConfigParser()
    settings_parser.read(config_file)

    global input_file, source_name, credentials_file, error_log, db_location, db_port, db_name

    input_file = settings_parser.get('Download', 'Location')
    source_name = settings_parser.get('Download', 'Source_name')
    error_log = settings_parser.get('Download', 'Error_log_location')

    credentials_file = settings_parser.get('Credentials', 'Location')
    db_location = settings_parser.get('Database', 'Location')
    db_port = int(settings_parser.get('Database', 'Port'))
    db_name = settings_parser.get('Database', 'DB')


def authenticate():
    """
    Authenticate
        1) database
        2) twitter api
    """
    # database
    global client, db, tweets, users, hashtags, sources

    client = MongoClient(db_location, db_port)
    db = client[db_name]
    tweets = db.tweets
    users = db.users
    hashtags = db.hashtags
    sources = db.source

    # twitter spi
    with open(credentials_file, "rb") as crd_file:
        crd_reader = csv.reader(crd_file)
        for row in crd_reader:
            cred_api = twitter.Api(consumer_key=row[0],
                                consumer_secret=row[1],
                                access_token_key=row[2],
                                access_token_secret=row[3])
            credentials.append(cred_api)
            expiration_time.append(0)


def download_tweets():
    """
    download the tweets from the provided data set
    """
    file_output = open(error_log, 'w')
    # TODO add support for network outage
    with open(input_file, "rb") as csv_file:
        data_reader = csv.reader(csv_file)
        i = 0
        current_api = get_active_api()
        for row in data_reader:
            i += 1
            print i
            tweet_id = row[0]
            user_id = row[1]
            found_tweet = tweets.find_one({"_id": int(tweet_id)})
            found_user = users.find_one({"_id": int(user_id)})
            try:
                if (found_tweet is None) and (found_user is None):
                    # download both
                    tweet_obj = current_api.GetStatus(id=tweet_id)
                    save_tweet(tweet_obj)

                    user_obj = tweet_obj.GetUser()
                    retrieved_user_id = int(user_obj.GetId())
                    if retrieved_user_id != int(user_id):
                        print "ERROR IN DATA: WRONG USER ID "+str(retrieved_user_id)+":"+str(user_id)
                        tweet_user = users.find_one({"_id": retrieved_user_id})
                        if tweet_user is None:
                            save_user(user_obj)

                    print "ttttttttttttttttuuuuuuuuuuuuuuuu"
                    print "User id " + str(retrieved_user_id)
                    print "Tweet id " + str(tweet_id)
                    data_set = dict()
                    data_set["source"] = source_name
                    data_set["tweet_id"] = tweet_id
                    sources.insert(data_set)
                elif (found_tweet is None) and found_user:
                    # download tweet
                    # this can happen when the user has appeared before
                    tweet_obj = current_api.GetStatus(id=tweet_id)
                    save_tweet(tweet_obj)

                    print "tttttttttttttttttttttttttttttttt"
                    print "User id " + str(user_id)
                    print "Tweet id " + str(tweet_id)
                    data_set = dict()
                    data_set["source"] = source_name
                    data_set["tweet_id"] = tweet_id
                    sources.insert(data_set)
                elif (found_user is None) and found_tweet:
                    # download user
                    user_obj = current_api.GetUser(user_id=user_id)
                    save_user(user_obj)

                    print "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu"
                    print "User id " + str(user_id)
                    print "Tweet id " + str(tweet_id)
                else:
                    print "tweet and user in db for id: "+str(tweet_id)
            except twitter.TwitterError as err:
                print "Twitter error:", err
                # file_output.write(str(i) + ', ERROR,' + str(err[0][0]['code']) + ',' + err[0][0]['message'] + '\n')
                try:
                    if err[0][0]['code'] == 88:
                        set_expired(current_api)
                        current_api = get_active_api()
                except:
                    print "Error: if err[0][0]['code'] == 88 failed"
            except requests.RequestException as err:
                print "HTTP Error: ", err
                file_output.write('HTTP Error:'+str(err))
            except:
                print "Unexpected error:", sys.exc_info()[0]
                file_output.write('UNEXPECTED ERROR: OH NO!!'+str(sys.exc_info()[0]))
                raise
    file_output.close()


def main():
    read_config(config_file)
    authenticate()
    download_tweets()

if __name__ == "__main__":
    main()
