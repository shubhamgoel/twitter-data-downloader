__author__ = 'shubham'

"""
update the source for tweets already downloaded using the dfreelon tunisia data set
"""

from pymongo import MongoClient

""" database connections """
# client = MongoClient('shackleton.gspp.berkeley.edu', 27017)
client = MongoClient('localhost', 27017)

db = client['arabspr']
tweets = db.tweets
sources = db.source


found_tweets = tweets.find()
for tweet in found_tweets:
    tag = dict()
    tag["source"] = "dfreelon #sidibouzid (Tunisia) (79,166 tweets)"
    tag["tweet_id"] = tweet["_id"]
    sources.insert(tag)
