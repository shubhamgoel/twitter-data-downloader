Tweet Data Set Reconstructor
========


This application can be used to reconstruct a twitter dataset which has tweet ids and the corresponding user ids. The app downloads meta data associated with the tweet status like text, creating date, location etc. The application is capable of translating tweets in foreign languages to english using the Google Translator API and export all the data as CSV files for further analysis.


Look how easy it is to use:

    $ source .virtualenv/arb/bin/activate
    
    $ nohup python download_tweet.py &

This will start downloading the tweet data corresponding to the dataset defined in the config file.

Features
---------

- Reconstruct twitter data
- Translate data in foreign languages
- Store data in normalized tables on a mongodb instance
- Export the normalized tables as CSV files
- Ability to download multiple data sets at the same time and remember groupings using a unique name for each dataset
- Config file for setting up the inputs for the scripts

Dependencies
----------

- Python 2.7.*
- ConfigParser
- twitter
- requests
- pymongo==2.6.3
- python-twitter==1.3.1
- requests==2.2.1
- requests-oauthlib==0.4.0
- simplejson==3.3.3

Usage
-------

Running the **download script**:

     $ source .virtualenv/arb/bin/activate
     
     $ nohup python download_tweet.py &

This will start downloading the tweets based on the data set defined in the config file. It will log the output to a file named nohup.out in the same folder as the script. It will also log the error file to the file location defined in the config file.


Running the **translate script**:

     $ source .virtualenv/arb/bin/activate
     
     $ nohup python translate_tweet.py &

This will translate the tweet text in a foreign language to english for the dataset defined in the config file under the header [Translate].

Running the **export script**:

     $ source .virtualenv/arb/bin/activate
     
     $ nohup python export_tweets.py &

This will export the tweets for a dataset to three different normalized files defined in the config files:

- Tweet_file
- User_file
- Hashtag_file

Contribute
----------

- Issue Tracker: [bitbucket.org/shubhamgoel/twitter-data-downloader/issues](bitbucket.org/shubhamgoel/twitter-data-downloader/issues)
- Source Code: [bitbucket.org/shubhamgoel/twitter-data-downloader](bitbucket.org/shubhamgoel/twitter-data-downloader)

Support
-------

If you are having issues, please let me know.
I can be reached at shubham[dot]goel[at]gmail.com

License
-------

The project is a copyright of [Shubham Goel](http://shubhamgoel.me), 2013.
