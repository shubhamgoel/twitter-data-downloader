# Written at dawn
# by Shubham Goel

__author__ = 'shubham@ischool.berkeley.edu'

from ConfigParser import SafeConfigParser
import csv
import time
import datetime
from calendar import timegm
import rfc822
import sys
from pymongo import MongoClient
from pymongo import errors

""" Global Variables """
config_file = 'config/config.ini'

""" data files """
source_name = None
export_tweet_file = None
export_user_file = None
export_hash_file = None
error_log = None

""" database connections """
db_location = None
db_port = None
db_name = None
client = None
db = None
tweets = None
users = None
hashtags = None
sources = None


def read_config(config_file):
    """
    reading configurations and updating the global variables
    """
    settings_parser = SafeConfigParser()
    settings_parser.read(config_file)

    global source_name, export_tweet_file, export_user_file, export_hash_file, error_log
    global db_location, db_port, db_name

    source_name = settings_parser.get('Export', 'Source_name')

    export_tweet_file = settings_parser.get('Export', 'Tweet_file')
    export_user_file = settings_parser.get('Export', 'User_file')
    export_hash_file = settings_parser.get('Export', 'Hashtag_file')
    error_log = settings_parser.get('Export', 'Error_log_location')

    db_location = settings_parser.get('Database', 'Location')
    db_port = int(settings_parser.get('Database', 'Port'))
    db_name = settings_parser.get('Database', 'DB')


def authenticate():
    """
    Authenticate
        1) database
    """
    # database
    global client, db, tweets, users, hashtags, sources

    client = MongoClient(db_location, db_port)
    db = client[db_name]
    tweets = db.tweets
    users = db.users
    hashtags = db.hashtags
    sources = db.source


def clean_text(input_text):
    """
    if text is not none then
        remove pipes
        remove new line character
        encode utf-8
    else return empty string
    """
    if input_text:
        cleaned_text = input_text
        cleaned_text = str(cleaned_text.encode('utf-8'))
        cleaned_text = ' '.join(cleaned_text.split())
        cleaned_text = cleaned_text.replace('\n', ' ')
        cleaned_text = cleaned_text.replace('|', '-')
        return cleaned_text
    else:
        return ""


def export_tweets():
    error_output = open(error_log, 'w')
    with open(export_tweet_file, 'wb') as csv_output_file:
        tweet_writer = csv.writer(csv_output_file, delimiter='|', quotechar='\\')
        # sort column title names in the CSV for consistency
        column_titles = [
            'tweet_id',
            'user_id',
            'text',
            'truncated',
            'favorite count',
            'retweet count',
            'lang',
            'source',
            'in reply to screen name',
            'in reply to user id',
            'in reply to status id',
            'place.full_name',
            'place.country',
            'place.place_type',
            'place.country_code',
            'place.name',
            'place.attributes',
            'place.x1',
            'place.y1',
            'place.x2',
            'place.y2',
            'place.x3',
            'place.y3',
            'place.x4',
            'place.y4',
            'coordinates.x',
            'coordinates.y',
            # 'coordinates.type',
            'year',
            'month',
            'day',
            'HH',
            'MM',
            'SS']

        '''
        Note about the data
        geo is the inverse of coordinate and is also depricated
        TODO no non-null value of location has been found
        favorited has been deprecated

        users, urls, user mentions and hash tags get their own csv
                'urls',#TODO separate table?
            'user mentions',#TODO separate table?
            'hash tags',#TODO check format

        '''
        tweet_writer.writerow(column_titles)
        data_set = sources.find({"source": source_name}, timeout=False)
        print str(datetime.datetime.now()) + " Number of tweets: "+str(data_set.count())
        i = 0
        try:
            for data in data_set:
                tweet_dict = tweets.find_one({'_id': int(data["tweet_id"])})
                i += 1
                print str(datetime.datetime.now()) + " tweet # "+str(i)
                tweet_list = list()
                # tweet_id
                tweet_list.append(tweet_dict["_id"])
                # user_id
                tweet_list.append(tweet_dict["user_id"])
                # text
                tweet_list.append(clean_text(tweet_dict["text"]))
                # truncated
                tweet_list.append(tweet_dict["truncated"])
                # favorite count
                tweet_list.append(tweet_dict["favorite_count"])
                # retweet count
                tweet_list.append(tweet_dict["retweet_count"])
                # lang
                tweet_list.append(tweet_dict["lang"])
                # source
                tweet_list.append(tweet_dict["source"].encode('utf-8'))
                # in reply to screen name
                tweet_list.append(tweet_dict["in_reply_to_screen_name"])
                # in reply to user id
                tweet_list.append(tweet_dict["in_reply_to_user_id"])
                # in reply to status id
                tweet_list.append(tweet_dict["in_reply_to_status_id"])

                if tweet_dict["place"]:
                    # place.full_name
                    tweet_list.append(tweet_dict["place"]["full_name"].encode('utf-8'))
                    # place.country
                    tweet_list.append(tweet_dict["place"]["country"])
                    # place.place_type
                    tweet_list.append(tweet_dict["place"]["place_type"])
                    # place.country_code
                    tweet_list.append(tweet_dict["place"]["country_code"])
                    # place.name
                    tweet_list.append(tweet_dict["place"]["name"].encode('utf-8'))
                    # place.attributes
                    tweet_list.append(tweet_dict["place"]["attributes"])
                    # place.x1, y1, x2, y2, x3, y3, x4, y4
                    for coord in tweet_dict["place"]["bounding_box"]["coordinates"][0]:
                        tweet_list.extend(coord)
                else:
                    list_none = [None] * 14
                    tweet_list.extend(list_none)

                if tweet_dict["coordinates"]:
                    # coordinates.x, y
                    tweet_list.extend(tweet_dict["coordinates"]["coordinates"])
                    # coordinates.type
                    # tweet_list.extend(tweet_dict["coordinates"]["type"])
                else:
                    list_none = [None] * 3
                    tweet_list.extend(list_none)

                # year, month, day, HH, MM, SS
                created_at = int(tweet_dict["created_at"])
                created_at_str = time.strftime('%Y,%m,%d,%H,%M,%S', time.gmtime(created_at))
                tweet_list.extend(created_at_str.split(','))
                tweet_writer.writerow(tweet_list)
        except errors.OperationFailure as err:
            print str(datetime.datetime.now()) + " operation error: ", err
            error_output.write('operation error:'+str(err))
        except:
            print str(datetime.datetime.now()) + " Unexpected error:", sys.exc_info()[0]
            error_output.write('UNEXPECTED ERROR: OH NO!!'+str(sys.exc_info()[0]))
            raise
    error_output.close()


def export_users():
    error_output = open(error_log, 'w')
    with open(export_user_file, 'wb') as csv_output_file:
        user_writer = csv.writer(csv_output_file, delimiter='|', quotechar='\\')
        column_titles = [
            'user id',
            'name',
            'screen name',
            'description',
            'profile image url',
            'protected',
            'utc offset',
            'time zone',
            'location',
            'url',
            'friend count',
            'listed count',
            'followers_count',
            'statuses_count',
            'favourites_count',
            'geo_enabled',
            'verified',
            'lang',
            'year',
            'month',
            'day',
            'HH',
            'MM',
            'SS']
        user_writer.writerow(column_titles)
        # todo: do not export all users
        found_users = users.find(timeout=False)
        print str(datetime.datetime.now()) + " Number of users: "+str(found_users.count())
        i = 0
        try:
            for user_dict in found_users:
                i += 1
                print str(datetime.datetime.now()) + " user # "+str(i)
                user_list = list()
                user_id = int(user_dict["_id"])
                # user id
                user_list.append(user_id)
                user_list.append(clean_text(user_dict["name"]))
                user_list.append(clean_text(user_dict["screen_name"]))

                user_list.append(clean_text(user_dict["description"]))

                user_list.append(user_dict["profile_image_url"])
                user_list.append(user_dict["protected"])
                user_list.append(user_dict["utc_offset"])
                user_list.append(user_dict["time_zone"])
                user_list.append(clean_text(user_dict["location"]))

                user_list.append(user_dict["url"].encode('utf-8') if user_dict["url"] else None)
                user_list.append(user_dict["friends_count"])
                user_list.append(user_dict["listed_count"])
                user_list.append(user_dict["followers_count"])
                user_list.append(user_dict["statuses_count"])
                user_list.append(user_dict["favourites_count"])
                user_list.append(user_dict["geo_enabled"])
                user_list.append(user_dict["verified"])
                user_list.append(user_dict["lang"])
                created_at = int(timegm(rfc822.parsedate(user_dict["created_at"])))
                created_at_str = time.strftime('%Y,%m,%d,%H,%M,%S', time.gmtime(created_at))
                user_list.extend(created_at_str.split(','))

                # print user_list
                user_writer.writerow(user_list)
        except errors.OperationFailure as err:
            print str(datetime.datetime.now()) + " operation error: ", err
            error_output.write('operation error:'+str(err))
        except:
            print str(datetime.datetime.now()) + " Unexpected error:", sys.exc_info()[0]
            error_output.write('UNEXPECTED ERROR: OH NO!!'+str(sys.exc_info()[0]))
            raise
    error_output.close()


def export_hash_tags():
    error_output = open(error_log, 'w')
    with open(export_hash_file, 'wb') as csv_output_file:
        hashtag_writer = csv.writer(csv_output_file, delimiter='|', quotechar='\\')
        column_titles = ['tweet_id', 'hashtag']
        hashtag_writer.writerow(column_titles)
        data_set = sources.find({"source": source_name}, timeout=False)
        i = 0
        try:
            for data in data_set:
                # tweet_dict = tweets.find_one({'_id': int(data["tweet_id"])})
                i += 1
                print str(datetime.datetime.now()) + " hash tag for tweet # " + str(i)
                hashtags_list = hashtags.find({'tweet_id': int(data["tweet_id"])})
                for hashtag_dict in hashtags_list:
                    hashtag_list = list()
                    hashtag_list.append(hashtag_dict["tweet_id"])
                    hashtag_list.append(hashtag_dict["hashtag"].encode('utf-8'))
                    hashtag_writer.writerow(hashtag_list)
        except errors.OperationFailure as err:
            print str(datetime.datetime.now()) + " operation error: ", err
            error_output.write('operation error:'+str(err))
        except:
            print str(datetime.datetime.now()) + " Unexpected error:", sys.exc_info()[0]
            error_output.write('UNEXPECTED ERROR: OH NO!!'+str(sys.exc_info()[0]))
            raise
    error_output.close()

# def export_urls():
# def export_user_mentions():


def main():
    read_config(config_file)
    authenticate()
    export_tweets()
    export_users()
    export_hash_tags()


if __name__ == "__main__":
    main()
