# Written by Shubham Goel while thinking about travelling to Japan

__author__ = 'shubham@ischool.berkeley.edu'

from ConfigParser import SafeConfigParser
import requests
import urllib
from pymongo import MongoClient
from pymongo import errors

config_file = 'config/config.ini'

""" data files """
source_name = None
error_log = None

""" database connections """
db_location = None
db_port = None
db_name = None
client = None
db = None
tweets = None
sources = None

""" google api """
api_key = None
languages_to_translate = []


def clean_text(input_text):
    """
    if text is not none then
        remove pipes
        remove new line character
        encode utf-8
    else return empty string
    """
    if input_text:
        cleaned_text = input_text
        cleaned_text = str(cleaned_text.encode('utf-8'))
        cleaned_text = ' '.join(cleaned_text.split())
        cleaned_text = cleaned_text.replace('\n', ' ')
        cleaned_text = cleaned_text.replace('|', '-')
        return cleaned_text
    else:
        return ""


def translate(sentence, language='en'):
    """
    GET https://www.googleapis.com/language/translate/v2?key=INSERT-YOUR-KEY&source=en&target=de&q=Hello%20world&q=My%20name%20is%20Jeff
    """
    sentence = urllib.quote_plus(sentence)
    translate_query = "https://www.googleapis.com/language/translate/v2?key=" + api_key + \
                      "&source=" + language + \
                      "&target=en&q=" + sentence
    r = requests.get(translate_query)
    response = r.json()
    try:
        translated_text = response['data']['translations'][0]['translatedText']
        return translated_text
    except:
        raise Exception(sentence, language, response)


def read_config(config_file):
    """
    reading configurations and updating the global variables
    """
    settings_parser = SafeConfigParser()
    settings_parser.read(config_file)

    global source_name, error_log, db_location, db_port, db_name, api_key

    source_name = settings_parser.get('Translate', 'Source_name')

    error_log = settings_parser.get('Translate', 'Error_log_location')

    db_location = settings_parser.get('Database', 'Location')
    db_port = int(settings_parser.get('Database', 'Port'))
    db_name = settings_parser.get('Database', 'DB')

    api_key = settings_parser.get('Translate', 'Key')

    languages = settings_parser.get('Translate', 'Languages_to_translate')
    languages_to_translate.extend(languages.split(','))


def authenticate():
    """
    Authenticate
        database
    """
    # database
    global client, db, tweets, sources

    client = MongoClient(db_location, db_port)
    db = client[db_name]
    tweets = db.tweets
    sources = db.source


def translate_tweets():
    """
    Translate the tweets for the provided source data set name
    """
    error_output = open(error_log, 'w')
    try:
        data_set = sources.find({"source": source_name}, timeout=False)
        print "Number of tweets: "+str(data_set.count())
        i = 0
        # start iterating through tweets
        for data in data_set:
            try:
                tweet_id = int(data["tweet_id"])
                tweet_dict = tweets.find_one({'_id': tweet_id})
                i += 1
                # check if already translated
                if "translated_text" in tweet_dict:
                    tweets.update({'_id': tweet_id}, {"$set": {'translated': True}}, upsert=False)
                    continue
                print i
                language = tweet_dict["lang"]
                print language
                if language in languages_to_translate:
                    if language == 'in':
                        language = 'id'
                    print language
                    tweet_text = clean_text(tweet_dict["text"])
                    translated_text = translate(tweet_text, language)
                    # tweet_dict["translated_text"] = translated_text
                    print translated_text
                    tweets.update({'_id': tweet_id}, {"$set": {'translated_text': translated_text}}, upsert=False)
                    tweets.update({'_id': tweet_id}, {"$set": {'translated': True}}, upsert=False)
            except Exception as err:
                print "translation error: ", err.args
                error_output.write('translation error: '+str(err))
            except errors.OperationFailure as err:
                print "operation error: ", err.args
                error_output.write('operation error:'+str(err))
            except:
                print "Unexpected error:", sys.exc_info()[0]
                error_output.write('UNEXPECTED ERROR: OH NO!!'+str(sys.exc_info()[0]))
                raise
    except errors.OperationFailure as err:
        print "operation error: ", err.args
        error_output.write('operation error:'+str(err))
    except:
        print "Unexpected error:", sys.exc_info()[0]
        error_output.write('UNEXPECTED ERROR: OH NO!!'+str(sys.exc_info()[0]))
        raise
    error_output.close()


def main():
    read_config(config_file)
    authenticate()
    translate_tweets()

if __name__ == "__main__":
    main()
