__author__ = 'shubham'

import csv
import requests
import urllib

# TODO move to another folder
api_key = "AIzaSyDT0m6L4zWufV5jDsbWHOEjpeFqgRh240E"


def translate(sentence, language='en'):
    """
    GET https://www.googleapis.com/language/translate/v2?key=INSERT-YOUR-KEY&source=en&target=de&q=Hello%20world&q=My%20name%20is%20Jeff
    """
    # sentence = sentence.replace('#', '%23')
    sentence = urllib.quote_plus(sentence)
    translate_query = "https://www.googleapis.com/language/translate/v2?key="+api_key+"&source="+language+"&target=en&q="+sentence
    r = requests.get(translate_query)
    response = r.json()
    try:
        translated_text = response['data']['translations'][0]['translatedText']
        return translated_text
    except:
        # TODO raise exception
        print "failed to translate:"
        print sentence
        print "reason"
        print response
        return None

input_file = 'data_file/text_to_translate.csv'
output_file = 'data_file/translated.csv'

with open(input_file, "rb") as csv_file:
    data_reader = csv.reader(csv_file)
    i = 0
    with open(output_file, 'wb') as csv_output_file:
        tweet_writer = csv.writer(csv_output_file)
        # tweet_writer = csv.writer(csv_output_file, delimiter='|', quotechar='\\')
        for row in data_reader:
            # print i
            i += 1
            language = row[0]
            original_text = ', '.join(row[1:])
            translated_text = translate(original_text, language)
            original_text = original_text.decode('utf-8')
            print language + "|" + original_text + "|" + translated_text
            # tweet_writer.writerow([language, original_text, translated_text])
