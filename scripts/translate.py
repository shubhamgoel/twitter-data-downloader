__author__ = 'shubham'

import requests
import urllib

# TODO move to another folder
api_key = "AIzaSyDT0m6L4zWufV5jDsbWHOEjpeFqgRh240E"


def detect_language(sentence):
    """
    GET https://www.googleapis.com/language/translate/v2/detect?key=INSERT-YOUR-KEY&q=Google%20Translate%20Rocks
    """
    language_query = "https://www.googleapis.com/language/translate/v2/detect?key="+api_key+"&q="+sentence
    r = requests.get(language_query)
    response = r.json()
    try:
        language = response['data']['detections'][0][0]['language']
        confidence = response['data']['detections'][0][0]['confidence']
        is_reliable = response['data']['detections'][0][0]['isReliable']

        return [language, confidence, is_reliable]
    except:
        # TODO raise exception
        print "failed to detect language for string:"
        print sentence
        print "reason"
        print response
        return None

    payload = {'key': 'value1', 'q': text}
    lang_detection_request = requests.get("https://www.googleapis.com/language/translate/v2/detect", params=payload)
    response = lang_detection_request.json()
    response["data"]["detections"][0][0]["language"]


def translate(sentence, language='en'):
    """
    GET https://www.googleapis.com/language/translate/v2?key=INSERT-YOUR-KEY&source=en&target=de&q=Hello%20world&q=My%20name%20is%20Jeff
    """
    # sentence = sentence.replace('#', '%23')
    sentence = urllib.quote_plus(sentence)
    translate_query = "https://www.googleapis.com/language/translate/v2?key="+api_key+"&source="+language+"&target=en&q="+sentence
    print translate_query
    r = requests.get(translate_query)
    response = r.json()
    print r.url
    try:
        translated_text = response['data']['translations'][0]['translatedText']
        print response
        return translated_text
    except:
        # TODO raise exception
        print "failed to translate:"
        print sentence
        print "reason"
        print response
        return None