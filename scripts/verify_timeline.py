__author__ = 'shubham'

import twitter
import csv
import time
import sys

# data files
file_name = 'data_file/sidibouzid_ids.csv'
output_file = 'data_file/tunisia_chrono.txt'
credentials_file = 'data_file/credentials.csv'

# Authenticate all credential
credentials = []  # stores all the authenticated credentials
expiration_time = []

with open(credentials_file, "rb") as crd_file:
    crd_reader = csv.reader(crd_file)
    for row in crd_reader:
        cred_api = twitter.Api(consumer_key=row[0],
                               consumer_secret=row[1],
                               access_token_key=row[2],
                               access_token_secret=row[3])
        credentials.append(cred_api)
        expiration_time.append(0)


# api.VerifyCredentials()


def set_expired(api):
    print "def set_expired:"
    cred_index = credentials.index(api)
    print "expired api index: "+str(cred_index)
    expiration_time[cred_index] = int(time.time())


def get_active_api():
    print "def get_active_api"
    min_expired_time = min(expiration_time)
    # 15 mins since expiration
    if (int(time.time())-min_expired_time) < 900:
        print "sleeping for: "+str(960-(int(time.time())-min_expired_time))+" sec"
        time.sleep(960-(int(time.time())-min_expired_time) + 60)
    cred_index = expiration_time.index(min_expired_time)
    return credentials[cred_index]


file_output = open(output_file, 'w')

with open(file_name, "rb") as csv_file:
    data_reader = csv.reader(csv_file)
    i = 0
    last_tweet_timestamp = 0
    current_api = get_active_api()
    for row in data_reader:
        i += 1
        print i
        tweet_id = row[0]
        try:
            tweet = current_api.GetStatus(id=tweet_id)
            text = tweet.GetText()
            text = text.encode('utf8')
            current_tweet_timestamp = tweet.GetCreatedAtInSeconds()
            if last_tweet_timestamp > current_tweet_timestamp:
                print "**************timeline break**************"
                file_output.write("**************timeline break**************")
            last_tweet_timestamp = current_tweet_timestamp
            print tweet.GetText()
            print tweet.GetCreatedAt()
            print "************************"
            file_output.write(str(i) + ',' + tweet.GetCreatedAt() + ',')
            file_output.write(text)
            file_output.write('\n')
        except twitter.TwitterError as err:
            print "Unexpected error:", err
            file_output.write(str(i) + ', ERROR,' + str(err[0][0]['code']) + ',' + err[0][0]['message'] + '\n')
            if err[0][0]['code'] == 88:
                set_expired(current_api)
                current_api = get_active_api()

file_output.close()

