__author__ = 'shubham'
import csv
import os
import twitter
import time

# file names
export_file = 'data_file/tweets_timeline.csv'
# error_log = 'data_file/tweets_error_log.txt'
credentials_file = 'data_file/credentials.csv'
error_log = 'data_file/bahrain_error_log.txt'

""" Authenticate all credential """
credentials = []  # stores all the authenticated credentials
expiration_time = []

with open(credentials_file, "rb") as crd_file:
    crd_reader = csv.reader(crd_file)
    for row in crd_reader:
        cred_api = twitter.Api(consumer_key=row[0],
                               consumer_secret=row[1],
                               access_token_key=row[2],
                               access_token_secret=row[3])
        credentials.append(cred_api)
        expiration_time.append(0)
# api.VerifyCredentials()


""" api switch management """


def set_expired(api):
    print "def set_expired:"
    cred_index = credentials.index(api)
    print "expired api index: "+str(cred_index)
    expiration_time[cred_index] = int(time.time())


def get_active_api():
    print "def get_active_api"
    min_expired_time = min(expiration_time)
    # 15 mins since expiration
    if (int(time.time())-min_expired_time) < 900:
        print "sleeping for: "+str(960-(int(time.time())-min_expired_time))+" sec"
        time.sleep(960-(int(time.time())-min_expired_time) + 60)
    cred_index = expiration_time.index(min_expired_time)
    return credentials[cred_index]


""" exporter """
current_api = get_active_api()
with open(export_file, 'wb') as csv_output_file:
    tweet_writer = csv.writer(csv_output_file)
    for file_name in os.listdir("data_set/"):
        if file_name.endswith(".csv"):
            file_name_w_dir = "data_set/" + file_name
            with open(file_name_w_dir, 'rb') as data_file:
                try:
                    first = next(data_file).decode()
                    tweet_id_first = int(first.split(',')[0])
                    print tweet_id_first
                    tweet_obj_first = current_api.GetStatus(id=tweet_id_first)
                    first_created_at = tweet_obj_first.GetCreatedAt()

                    data_file.seek(-1024, 2)
                    last = data_file.readlines()[1].decode()
                    tweet_id_last = int(last.split(',')[0])
                    print tweet_id_last
                    tweet_obj_last = current_api.GetStatus(id=tweet_id_last)
                    last_created_at = tweet_obj_last.GetCreatedAt()
                    to_write = [file_name, first_created_at, last_created_at]
                    tweet_writer.writerow(to_write)
                except twitter.TwitterError as err:
                    print "Twitter error:", err
                    if err[0][0]['code'] == 88:
                        set_expired(current_api)
                        current_api = get_active_api()


# tweet_writer.writerow(column_titles)
